#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , M8195A()
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_STOPbutton_clicked()
{
    M8195A.stopGen();
}


void MainWindow::on_RUNbutton_clicked()
{
    M8195A.run();
}

void MainWindow::on_loadWaveformAndSeqTable_clicked()
{
    int loops = ui->loopNb->value();

    double excite = ui->excTime->value();
    ViConstString chan = "1";

    double Fstart = ui->scanStartFreq->value();
    double Fend = ui->scanStopFreq->value();
    int stepNb = ui->scanStepNb->value();

    auto frequencies = linearFreqArray(Fstart*MHz_M8195A, Fend*MHz_M8195A, stepNb);

    ViStatus status = M8195A.loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, excite*microsec_M8195A, loops);
}

void MainWindow::on_triggerMode_currentIndexChanged(const QString &mode)
{
    M8195A.setTriggermode(mode);
}
